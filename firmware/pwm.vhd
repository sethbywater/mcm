library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Pwm is
	port (
		clk			: in  std_logic;
		duty_cycle	: in  std_logic_vector(7 downto 0);
		pwm			: out std_logic;
	);
end Pwm;

architecture rtl of Pwm is
	signal temp_pwm: std_logic := '0';
begin
	process( sensitivity_list )
		signal cnt: std_logic_vector(7 downto 0);
	begin
		if rising_edge(clk) then
			cnt <= cnt + 1;
			temp_pwm <= cnt =< duty_cycle;
		end if;
	end process;
	pwm <= temp_pwm;

end rtl;